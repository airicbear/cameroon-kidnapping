# Cameroon Kidnapping
Afro/Asian 12  
11/7/2018  
Mr. Booth (Substitute)  

## Who

### Kidnapped

- 78 school children (released)
- 1 school principal
- 1 school teacher
- 1 driver (released)

### Kidnappers

- "Amba Boys" (suspected, but denied by AIPC)

## What

- Kidnapping of Bamenda students

## When

- Sunday 11/4/2018

## Where

### From

- Presbyterian Secondary School in Bamenda

### To

- Presbyterian Church of Cameroon in Bafut, about 24km (15 miles) from Bamenda

## Why

- Ambazonia--an independent state.
	- An Ambazonian movement is causing unrest in the area. The kidnappers may be working for the movement, or taking advantage of the unrest which it has caused.
- The government refuses to translate French laws to other languages, such as English.

## How

- "Students were abandoned in one of their buildings in the town of Bafut, about 24km (15 miles) from Bamenda."
- Students and staff are defenseless against the gunmen.

## References
- [BBC News](https://www.bbc.com/news/world-africa-46122678)